package AerialVehicles;

import Entities.Coordinates;
import Enums.FlightStatus;
import Enums.MissionType;
import OutputHandler.ConsoleWriter;
import java.util.List;

public abstract class AerialVehicle {

    private int flightHoursLastFix;
    private FlightStatus flightStatus;
    private Coordinates sourceBaseLocation;
    private int maxHoursRequiresRepair;
    private List<MissionType> legalMissions;

    public List<MissionType> getLegalMissions() {
        return legalMissions;
    }

    public Coordinates getSourceBaseLocation() {
        return sourceBaseLocation;
    }

    public void setLegalMissions(List<MissionType> legalMissions) {
        this.legalMissions = legalMissions;
    }

    public void setMaxHoursRequiresRepair(int maxHoursRequiresRepair) {
        this.maxHoursRequiresRepair = maxHoursRequiresRepair;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    public void setFlightHoursLastFix(int flightHoursLastFix) {
        this.flightHoursLastFix = flightHoursLastFix;
    }

    public AerialVehicle(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        this.flightHoursLastFix = flightHoursLastFix;
        this.flightStatus = flightStatus;
        this.sourceBaseLocation = sourceBaseLocation;
    }

    public void flyTo(Coordinates destination) {
        if (this.flightStatus == FlightStatus.READY) {
            ConsoleWriter.write("Flying to: " + destination.toString());
            this.setFlightStatus(FlightStatus.IN_FLIGHT);
        } else if (this.flightStatus == FlightStatus.NOT_READY) {
            ConsoleWriter.write("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination) {
        ConsoleWriter.write("Landing on: " + destination.toString());
        this.check(this.maxHoursRequiresRepair);
    }

    public void check(int hoursRequiresRepair) {
        if (this.flightHoursLastFix >= hoursRequiresRepair) {
            this.setFlightStatus(FlightStatus.NOT_READY);
        } else {
            this.setFlightStatus(FlightStatus.READY);
        }
    }

    public void repair() {
        this.setFlightHoursLastFix(0);
        this.setFlightStatus(FlightStatus.READY);
    }
}
