package AerialVehicles.RemotePlanes;

import Enums.FlightStatus;
import Entities.Coordinates;
import Enums.MissionType;
import java.util.ArrayList;
import java.util.List;

public class Kochav extends Hermes{
    public Kochav(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
        List<MissionType> legalMissions = new ArrayList<>();
        legalMissions.add(MissionType.ATTACK);
        legalMissions.add(MissionType.BDA);
        legalMissions.add(MissionType.INTELLIGENCE);
        this.setLegalMissions(legalMissions);
    }
}
