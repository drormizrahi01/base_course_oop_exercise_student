package AerialVehicles.RemotePlanes;

import AerialVehicles.AerialVehicle;
import Enums.FlightStatus;
import Entities.Coordinates;
import OutputHandler.ConsoleWriter;

public abstract class RemotePlane extends AerialVehicle {

    public RemotePlane(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
    }

    public String hoverOverLocation(Coordinates destination) {
        this.setFlightStatus(FlightStatus.IN_FLIGHT);
        String hoverOverMessage = "Hovering Over: " + destination.toString();
        ConsoleWriter.write(hoverOverMessage);

        return hoverOverMessage;
    }
}
