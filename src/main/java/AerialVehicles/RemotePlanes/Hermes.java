package AerialVehicles.RemotePlanes;

import Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Hermes extends RemotePlane{
    private final int HOURS_REQUIRES_REPAIR = 100;

    public Hermes(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
        this.setMaxHoursRequiresRepair(HOURS_REQUIRES_REPAIR);
    }
}
