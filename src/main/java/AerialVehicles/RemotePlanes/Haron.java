package AerialVehicles.RemotePlanes;

import Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Haron extends RemotePlane{
    private final int HOURS_REQUIRES_REPAIR = 150;

    public Haron(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
        this.setMaxHoursRequiresRepair(HOURS_REQUIRES_REPAIR);
    }
}
