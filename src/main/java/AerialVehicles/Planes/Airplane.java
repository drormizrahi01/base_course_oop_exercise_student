package AerialVehicles.Planes;

import AerialVehicles.AerialVehicle;
import Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Airplane extends AerialVehicle {
    private final int HOURS_REQUIRES_REPAIR = 250;

    public Airplane(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
        this.setMaxHoursRequiresRepair(HOURS_REQUIRES_REPAIR);
    }
}
