package AerialVehicles.Planes;


import Enums.FlightStatus;
import Entities.Coordinates;
import Enums.MissionType;
import java.util.ArrayList;
import java.util.List;

public class F16 extends Airplane {
    public F16(int flightHoursLastFix, FlightStatus flightStatus, Coordinates sourceBaseLocation) {
        super(flightHoursLastFix, flightStatus, sourceBaseLocation);
        List<MissionType> legalMissions = new ArrayList<>();
        legalMissions.add(MissionType.ATTACK);
        legalMissions.add(MissionType.BDA);
        this.setLegalMissions(legalMissions);
    }
}
