package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissionType;
import Exceptions.AerialVehicleNotCompatibleException;
import OutputHandler.ConsoleWriter;

public abstract class Mission implements MissionActions{
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle executeVehicle;
    private MissionType missionType;

    public AerialVehicle getExecuteVehicle() {
        return executeVehicle;
    }

    public String getPilotName() {
        return pilotName;
    }

    public Mission(Coordinates destination, String pilotName, AerialVehicle executeVehicle, MissionType missionType) throws AerialVehicleNotCompatibleException {
        this.destination = destination;
        this.pilotName = pilotName;
        this.executeVehicle = executeVehicle;
        this.missionType = missionType;
        this.checkVehicleLegalMission();
    }

    @Override
    public void begin() {
        ConsoleWriter.write("Beginning Mission!");
        this.executeVehicle.flyTo(this.destination);
    }

    @Override
    public void cancel() {
        ConsoleWriter.write("Abort Mission!");
        this.executeVehicle.land(this.executeVehicle.getSourceBaseLocation());
    }

    @Override
    public void finish() {
        ConsoleWriter.write(this.executeMission());
        this.executeVehicle.land(this.destination);
        ConsoleWriter.write("Finish Mission!");
    }

    public void checkVehicleLegalMission() throws AerialVehicleNotCompatibleException {
        if (!this.executeVehicle.getLegalMissions().contains(this.missionType)) {
            throw new AerialVehicleNotCompatibleException("Aerial Vehicle can't perform " + this.missionType + " mission");
        }
    }

    public abstract String executeMission();
}
