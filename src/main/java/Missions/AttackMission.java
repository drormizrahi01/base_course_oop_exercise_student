package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissileType;
import Enums.MissionType;
import Exceptions.AerialVehicleNotCompatibleException;

public class AttackMission extends Mission{
    private String target;
    private int missileNumber;
    private MissileType missileType;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle executeVehicle, String target, int missileNumber, MissileType missileType) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, executeVehicle, MissionType.ATTACK);
        this.target = target;
        this.missileNumber = missileNumber;
        this.missileType = missileType;
    }

    @Override
    public String executeMission() {
        String vehicleType = this.getExecuteVehicle().getClass().getName().substring(this.getExecuteVehicle().getClass().getName().lastIndexOf('.') + 1);
        return this.getPilotName() + ": " + vehicleType + " Attacking suspect " + this.target + " with: " + this.missileType + "X" + this.missileNumber;
    }
}
