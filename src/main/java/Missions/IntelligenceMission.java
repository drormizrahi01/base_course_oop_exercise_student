package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;
import Enums.MissionType;
import Enums.SensorType;
import Exceptions.AerialVehicleNotCompatibleException;

public class IntelligenceMission extends Mission{
    private SensorType sensorType;
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle executeVehicle, SensorType sensorType, String region) throws AerialVehicleNotCompatibleException {
        super(destination, pilotName, executeVehicle, MissionType.INTELLIGENCE);
        this.sensorType = sensorType;
        this.region = region;
    }

    @Override
    public String executeMission() {
        String vehicleType = this.getExecuteVehicle().getClass().getName().substring(this.getExecuteVehicle().getClass().getName().lastIndexOf('.') + 1);
        return this.getPilotName() + ": " + vehicleType + " Collecting Data in " + this.region + " with: sensor type: " + this.sensorType;
    }
}
