package Missions;

public interface MissionActions {
    void begin();
    void cancel();
    void finish();
}
