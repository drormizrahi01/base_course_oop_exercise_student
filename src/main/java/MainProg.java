import AerialVehicles.Planes.F15;
import Enums.FlightStatus;
import AerialVehicles.RemotePlanes.Kochav;
import Entities.Coordinates;
import Enums.MissileType;
import Exceptions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Enums.CameraType;
import OutputHandler.ConsoleWriter;

public class MainProg {
    public static void main(String[] args) {
        try {
            F15 raam = new F15(10, FlightStatus.IN_FLIGHT, new Coordinates(1.1, 5.5));
            Kochav k1 = new Kochav(5, FlightStatus.READY, new Coordinates(0.1, 3.7));
            BdaMission mission2 = new BdaMission(new Coordinates(5.5, 10.9), "liav", k1, "house", CameraType.NIGHT_VISION);
            AttackMission mission3 = new AttackMission(new Coordinates(0.1,0.2), "omer", raam, "hizbala", 500, MissileType.PYTHON);
            mission2.begin();
            mission2.finish();
            System.out.println("---------------->");
            mission3.begin();
            mission3.finish();
            System.out.println("---------------->");
            BdaMission mission1 = new BdaMission(new Coordinates(5.5, 10.9), "dror", raam, "house", CameraType.NIGHT_VISION);
            mission1.begin();
            mission1.finish();
        } catch (AerialVehicleNotCompatibleException error) {
            ConsoleWriter.write(error.getMessage());
        }
    }
}
